#!/bin/bash

f_dir=`ls $pwd`

arch=$1
f_tar=`tar tvf $arch`

for f in $f_dir; do
	if [[ $f_tar == *$f* ]]
	then
		echo $f ' is in archive'
	else
		echo $f ' is not in archive'
		if [[ $f != *$arch* ]]
		then
			tar rvf $arch $f
			echo $f ' is added to archive' $arch
		fi
	fi
done

tar cvzf $arch.gz $arch

exit 0
