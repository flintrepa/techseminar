#!/bin/bash

directory=$1
files=`ls $directory`

for f1 in $files; do
	for f2 in $files; do
		if [[ -z $(diff $f1 $f2) && $f1 != $f2 ]]; then
			echo $f1 $f2
		fi 
	done
done
